<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" media="screen" href="style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Motorrad - Liveticker</title>
</head>
<body>
<div id="container" style="margin-top:30px;">
<div id="header" style="margin-top:0px;">
<div id="headercapitle">
<h2>Motorrad - Liveticker</h2>
</div>
</div>
<hr color="#FFFFFF" style="margin-top:-10px;"/>
<div id="content">
</div>
<div id="footer">
<div id="footertext">
<p align="center"><b>2012 &copy; by Strasser Stefan</b></p>
</div>
</div>
</div>
</body>
</html>