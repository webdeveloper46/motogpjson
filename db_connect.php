<?php
	class DB_CONNECT {

		function __construct() {
			$this->connect();
		}

		function __destruct() {
			$this->close();
		}

		function connect() {
			require_once __DIR__ . '/db_config.php';
			$con = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die ("Verbindung zur Datenbank konnte nicht hergestellt werden!");
			mysql_set_charset('utf8',$con);
			$db = mysql_select_db(DB_DATABASE) or die ("Datenbank konnte nicht ausgewählt werden!");
			return $con;
		}

		function close() {
			mysql_close();
		}

	}
?>